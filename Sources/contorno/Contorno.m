#import "Contorno/Contorno.h"

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif

#include "cmark.h"
#include "node.h"

NSURL *getUrl(cmark_node *node);
NSString *getString(cmark_node *node);
NSString *const ContornoLinkTitleAttributeName = @"ContornoLinkTitleAttributeName";
NSString *const ContornoEmphasisAttributeName = @"ContornoEmphasisAttributeName";
NSString *const ContornoCodeblockAttributeName = @"ContornoCodeblockAttributeName";
NSString *const ContornoStrongEmphasisAttributeName = @"ContornoStrongEmphasisAttributeName";

@interface Link : NSObject
@property (nonatomic, readonly) NSURL *url;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSRange range;
- (instancetype)initWithUrl:(NSURL *)url title:(NSString *)title range:(NSRange)range;
@end

@implementation Link
- (instancetype)initWithUrl:(NSURL *)url title:(NSString *)title range:(NSRange)range {
    self = [super init];
    if (self) {
        _url = url;
        _title = title;
        _range = range;
    }
    return self;
}
@end

@interface Emphasis : NSObject
@property (nonatomic, readonly) BOOL isStrong;
@property (nonatomic, readonly) NSRange range;
- (instancetype)initWithStrong:(BOOL)strong range:(NSRange)range;
@end

@implementation Emphasis
- (instancetype)initWithStrong:(BOOL)strong range:(NSRange)range {
    self = [super init];
    if (self) {
        _isStrong = strong;
        _range = range;
    }
    return self;
}
@end


@interface Contorno ()
@property (nonatomic, readonly) NSMutableAttributedString *attributedString;
@property (nonatomic, readonly) NSMutableArray *links;
@property (nonatomic, readonly) NSMutableArray *emphases;
@property (nonatomic, readwrite) BOOL firstParagraph;
@end

@implementation Contorno

- (instancetype)init {
    self = [super init];
    if (self) {
        _attributedString = [[NSMutableAttributedString alloc] init];
        _links = [[NSMutableArray alloc] init];
        _emphases = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSAttributedString *)parse:(NSString *)commonMark format:(OutputFormat)format
{
    const char *c_commonMark = [commonMark UTF8String];
    cmark_node *root = cmark_parse_document(c_commonMark, strlen(c_commonMark), CMARK_OPT_SMART);
    switch(format) {
        case OutputFormatAttributed: {
            cmark_event_type ev_type;
            cmark_node *cur;
            cmark_iter *iter = cmark_iter_new(root);
            while ((ev_type = cmark_iter_next(iter)) != CMARK_EVENT_DONE) {
                cur = cmark_iter_get_node(iter);
                [self handleNode:cur eventType:ev_type];
            }
            cmark_iter_free(iter);
            cmark_node_free(root);
            return [self.attributedString copy];
        }
        case OutputFormatHTML: {
            char *html = cmark_render_html(root, CMARK_OPT_DEFAULT);
            NSString *string = [NSString stringWithCString:html encoding:NSUTF8StringEncoding];
            cmark_node_free(root);
            free(html);
            return [[NSAttributedString alloc] initWithString:string];
        }
        default:
            NSAssert(false, @"unhandled format");
            return nil;
    }
}

- (void) handleNode:(cmark_node *)node eventType:(cmark_event_type) ev_type
{
    bool entering = (ev_type == CMARK_EVENT_ENTER);

    switch (node->type) {
        case CMARK_NODE_PARAGRAPH: {
            if (entering) {
                if (!self.firstParagraph) {
                    self.firstParagraph = true;
                } else {
                    [self.attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:nil]];
                }
            }
            break;
        }
        case CMARK_NODE_SOFTBREAK:
        case CMARK_NODE_LINEBREAK: {
            [self.attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:nil]];
            break;
        }

        case CMARK_NODE_TEXT:
        case CMARK_NODE_CODE_BLOCK:
        {
            NSString *text = getString(node);
            if (text) {
                NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
                if (node->type == CMARK_NODE_CODE_BLOCK) {
                    attributes[ContornoCodeblockAttributeName] = @(YES);
                }
                [self.attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:text attributes:attributes]];
            }
            break;
        }

        case CMARK_NODE_INLINE_HTML:
        case CMARK_NODE_CODE:
            break;

        case CMARK_NODE_EMPH:
        case CMARK_NODE_STRONG:
            if (entering) {
                [self.emphases addObject:[[Emphasis alloc] initWithStrong:node->type ==CMARK_NODE_STRONG range:NSMakeRange(self.attributedString.length, 0)]];
            } else {
                Emphasis *emphasis = [self.emphases lastObject];
                if (emphasis) {
                    [self.emphases removeLastObject];
                    NSString *attrName = emphasis.isStrong ? ContornoStrongEmphasisAttributeName : ContornoEmphasisAttributeName;
                    [self.attributedString addAttributes:@{ attrName: @(YES) }
                                                   range:NSMakeRange(emphasis.range.location,
                                                         self.attributedString.length - emphasis.range.location)];
                }
            }
            break;

        case CMARK_NODE_LINK: {
            if (entering) {
                NSURL *url = getUrl(node);
                NSAssert(url, @"unable to get URL for node: \"%d:%d-%d:%d\"",
                    cmark_node_get_start_line(node), cmark_node_get_start_column(node),
                    cmark_node_get_end_line(node), cmark_node_get_end_column(node));

                const char *titleCString = cmark_node_get_title(node);
                NSString *title = titleCString ? [NSString stringWithUTF8String:titleCString] : nil;

                [self.links addObject:[[Link alloc] initWithUrl:url title:title range:NSMakeRange(self.attributedString.length, 0)]];
            } else {
                Link *link = [self.links lastObject];
                if (link) {
                    [self.links removeLastObject];
                    [self.attributedString addAttributes:@{
                       NSLinkAttributeName: [link.url copy],
                       ContornoLinkTitleAttributeName: link.title
                    } range:NSMakeRange(link.range.location, self.attributedString.length-link.range.location)];
                }
            }
        }
        default:
            break;
    }
}

NSString *getString(cmark_node *node)
{
    const char *literal = cmark_node_get_literal(node);
    if (literal) {
        return [NSString stringWithUTF8String:literal];
    } else {
        return nil;
    }
}

NSURL *getUrl(cmark_node *node) {
    const char *urlCString = cmark_node_get_url(node);
    if (urlCString) {
        NSString *urlString = [NSString stringWithUTF8String:urlCString];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        if (url) {
            return url;
        } else {
            // fallback to escaping input
            NSMutableCharacterSet *cset = [NSCharacterSet URLQueryAllowedCharacterSet].mutableCopy;
            [cset addCharactersInString:@"#"];
            return [[NSURL alloc] initWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:cset]];
        }
    } else {
        return nil;
    }
}

@end
