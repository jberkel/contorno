#import <Foundation/Foundation.h>

//! Project version number for Contorno.
FOUNDATION_EXPORT double ContornoVersionNumber;

//! Project version string for Contorno.
FOUNDATION_EXPORT const unsigned char ContornoVersionString[];

extern NSString * _Nonnull const ContornoLinkTitleAttributeName;
extern NSString * _Nonnull const ContornoEmphasisAttributeName;
extern NSString * _Nonnull const ContornoStrongEmphasisAttributeName;
extern NSString * _Nonnull const ContornoCodeblockAttributeName;

typedef NS_ENUM(NSInteger, OutputFormat) {
    OutputFormatAttributed,
    OutputFormatHTML,
};

@interface Contorno: NSObject
- (nonnull NSAttributedString *)parse:(nonnull NSString *)commonMark format:(OutputFormat)format;
@end
