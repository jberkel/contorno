import UIKit
import Contorno

class DetailViewController: UIViewController {
    @IBOutlet weak var detailDescriptionLabel: UILabel!

    var detailItem: CommonMark? {
        didSet {
            self.configureView()
        }
    }

    func configureView() {
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.description
            }

            if let content = try? String(contentsOf: detail.path),
               let textView = view as? UITextView {

                textView.attributedText = makeAttributed(string: content)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    private func makeAttributed(string: String) -> NSAttributedString {
        let contorno = Contorno()
        let attributedString = contorno.parse(string, format: .attributed)
        let description = attributedString.string.replacingOccurrences(of: "%", with: "%%")

        NSLog("attributed=\(description)")

        return attributedString
    }
}
