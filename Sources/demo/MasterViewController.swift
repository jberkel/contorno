import UIKit

struct CommonMark : CustomStringConvertible {
    let name: String
    let path: URL

    var description: String {
        name
    }
}

class MasterViewController: UITableViewController {
    var detailViewController: DetailViewController? = nil
    var files = [CommonMark]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

        if let path = Bundle.main.url(forResource: "CommonMark", withExtension: nil)?.path,
            let contents = try? FileManager.default.contentsOfDirectory(atPath: path) {
                self.files = contents.map {
                    CommonMark(name: $0, path: URL(fileURLWithPath: path).appendingPathComponent($0))
                }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let file = files[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

                controller.detailItem = file
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        files.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let file = files[indexPath.row]
        cell.textLabel!.text = file.name
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        false
    }
}

