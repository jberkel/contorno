//
//  ContornoTests.swift
//  Wikiloc
//
//  Created by Jan Berkel on 23/10/2021.
//  Copyright © 2021 wikiloc. All rights reserved.
//

import Foundation
import XCTest
import contorno

class ContornoTest: XCTestCase {
    var subject: Contorno!

    override func setUpWithError() throws {
        subject = Contorno()
    }

    func testParseHardLineBreak() throws {
        let attributed = subject.parse("foo  \nbar", format: .attributed)
        XCTAssertEqual("foo\nbar", attributed.string)
    }

    func testParseSoftLineBreak() throws {
        let attributed = subject.parse("foo\nbar", format: .attributed)
        XCTAssertEqual("foo\nbar", attributed.string)
    }

    func testParseHeader1() throws {
        let attributed = subject.parse("# Header1", format: .attributed)
        XCTAssertEqual("Header1", attributed.string)
    }

    func testParseHeader2() throws {
        let attributed = subject.parse("## Header2", format: .attributed)
        XCTAssertEqual("Header2", attributed.string)
    }

    func testParseHeader3() throws {
        let attributed = subject.parse("### Header3", format: .attributed)
        XCTAssertEqual("Header3", attributed.string)
    }

    func testParseListItem() throws {
        let attributed = subject.parse(" - list item", format: .attributed)
        XCTAssertEqual("list item", attributed.string)
    }

    func testParseImageWithoutDescription() throws {
        let attributed = subject.parse("![](/url \"title\")", format: .attributed)
        XCTAssertEqual("", attributed.string)
    }

    func testRawHTML() throws {
        let attributed = subject.parse("<p>Foo</p>", format: .attributed)
        XCTAssertEqual("", attributed.string)
    }

    func testSimpleCodeSpan() throws {
        let attributed = subject.parse("`foo`", format: .attributed)
        XCTAssertEqual("", attributed.string)
    }

    func testIndentCodeBlock() throws {
        let attributed = subject.parse("\n    print('foo');\n", format: .attributed)
        XCTAssertEqual(attributed.string, "print('foo');\n")
        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoCodeblockAttributeName),
                                         at: 0, effectiveRange: &range)
        XCTAssertEqual(value as? Bool, true)
        XCTAssertEqual(0, range.location)
        XCTAssertEqual(14, range.length)
    }

    func testFencedCodeSpan() throws {
        let attributed = subject.parse("```\nprint('foo');\n```", format: .attributed)
        XCTAssertEqual(attributed.string, "print('foo');\n")
        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoCodeblockAttributeName),
                                         at: 0, effectiveRange: &range)
        XCTAssertEqual(value as? Bool, true)
        XCTAssertEqual(0, range.location)
        XCTAssertEqual(14, range.length)
    }

    func testHrule() throws {
        let attributed = subject.parse("***\n", format: .attributed)
        XCTAssertEqual("", attributed.string)
    }

    func testBlankLines() throws {
        let attributed = subject.parse("foo\n\n\n\nbar\n", format: .attributed)
        XCTAssertEqual("foo\n\nbar", attributed.string)
    }

    func testSimpleLink() throws {
        let attributed = subject.parse("[foo](bar)", format: .attributed)
        XCTAssertEqual("foo", attributed.string)
        let url = attributed.attribute(.link, at: 0, effectiveRange: nil) as? URL
        XCTAssertEqual("bar", url?.absoluteString)
    }

    func testLinkTitle() throws {
        let attributed = subject.parse("[foo](bar 'title')", format: .attributed)
        XCTAssertEqual("foo", attributed.string)
        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoLinkTitleAttributeName),
                                         at: 0, effectiveRange: &range)
        XCTAssertNotNil(value)
        XCTAssertEqual("title", value as? String)
        XCTAssertEqual(0, range.location)
        XCTAssertEqual(3, range.length)
    }


    func testLinkWithQuotes() throws {
        let attributed = subject.parse("[foo](bar\\(baz\\))", format: .attributed)
        XCTAssertEqual("foo", attributed.string)
    }

    func testLinkWithFragment() throws {
        let attributed = subject.parse("[foo](bar#fragment)", format: .attributed)
        let url = attributed.attribute(.link, at: 0, effectiveRange: nil) as? URL
        XCTAssertNotNil(url)
        XCTAssertEqual("fragment", url?.fragment)
    }

    func testLinkWithPercentEscapes() throws {
        let attributed = subject.parse("[foo](</a%C3%A7ougueiro>)", format: .attributed)
        let url = attributed.attribute(.link, at: 0, effectiveRange: nil) as? URL
        XCTAssertNotNil(url)
        XCTAssertEqual("/açougueiro", url?.path)
    }

    func testLinkWithAngleBrackets() throws {
        let attributed = subject.parse("[foo](</bar/space/>)", format: .attributed)
        XCTAssertEqual(attributed.string, "foo")
        var range = NSRange()
        let url = attributed.attribute(.link, at: 0, effectiveRange: &range) as? URL
        XCTAssertNotNil(url)
        XCTAssertEqual("/bar/space", url?.path)
        XCTAssertEqual(0, range.location)
        XCTAssertEqual(3, range.length)
    }

    func testEmphasis() throws {
        let attributed = subject.parse("Yo, *Emphasis*", format: .attributed)
        XCTAssertEqual(attributed.string, "Yo, Emphasis")
        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoEmphasisAttributeName),
                                         at: 4, effectiveRange: &range)
        XCTAssertNotNil(value)
        XCTAssertEqual(4, range.location)
        XCTAssertEqual(8, range.length)
    }

    func testStrongEmphasis() throws {
        let attributed = subject.parse("Yo, **StrongEmphasis**", format: .attributed)
        XCTAssertEqual(attributed.string, "Yo, StrongEmphasis")
        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoStrongEmphasisAttributeName),
                                         at: 4, effectiveRange: &range)
        XCTAssertNotNil(value)
        XCTAssertEqual(4, range.location)
        XCTAssertEqual(14, range.length)
    }

    func testEmphasisLink() throws {
        let attributed = subject.parse("*[link](/path)*", format: .attributed)
        XCTAssertEqual(attributed.string, "link")
        let url = attributed.attribute(.link, at: 0, effectiveRange: nil) as? URL
        XCTAssertEqual("/path", url?.path)

        var range = NSRange()
        let value = attributed.attribute(NSAttributedString.Key(rawValue: ContornoEmphasisAttributeName),
                                         at: 0, effectiveRange: &range)
        XCTAssertNotNil(value)
        XCTAssertEqual(0, range.location)
        XCTAssertEqual(4, range.length)
    }

    func testSmartQuotesAreEnabled() throws {
        let attributed = subject.parse("It's great", format: .attributed)
        XCTAssertEqual(attributed.string, "It’s great")
    }

    func testParseHaus() throws {
        let result = subject.parse(try loadFixture(name: "Haus"), format: .attributed)
        let expected = try loadFixture(name: "Haus_expected", extension: "txt").trimmingCharacters(in: .newlines)
        XCTAssertEqual(expected, result.string)
    }

    func testParseSehen() throws {
        let result = subject.parse(try loadFixture(name: "sehen"), format: .attributed)
        let expected = try loadFixture(name: "sehen_expected", extension: "txt").trimmingCharacters(in: .newlines)
        XCTAssertEqual(expected, result.string)
    }

    func testEmailAutoLink() throws {
        let attributed = subject.parse("<foo@example.com>", format: .attributed)
        XCTAssertEqual("foo@example.com", attributed.string)
        let url = attributed.attribute(.link, at: 0, effectiveRange: nil) as? URL
        XCTAssertEqual("mailto:foo@example.com", url?.absoluteString)
    }

    func testParseToHTML() throws {
        let result = subject.parse(try loadFixture(name: "sehen"), format: .HTML)
        let expected = try loadFixture(name: "sehen_expected", extension: "html")
        XCTAssertEqual(result.string, expected)
    }

    func loadFixture(name: String, extension: String = "md") throws -> String {
        let path = Bundle.module.url(forResource: name, withExtension: `extension`, subdirectory: "fixtures")?.path
        XCTAssertNotNil(path)
        return try String(contentsOfFile: path!)
    }
}
