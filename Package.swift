// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "contorno",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "contorno",
            targets: ["contorno"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
          .target(
            name: "libcmark",
            path: "Sources/cmark/src",
            exclude: [
              "main.c",
              "cmarkConfig.cmake.in",
              "cmark_version.h.in",
              "libcmark.pc.in",
              "config.h.in",
              "scanners.re",
              "CMakeLists.txt",
              "case_fold_switch.inc",
              "entities.inc"
            ],
            publicHeadersPath: "./",
            cSettings: [
                .headerSearchPath("../../cmark-config")
            ]
        ),
        .target(
            name: "contorno",
            dependencies: ["libcmark"],
            publicHeadersPath: "Contorno",
            cSettings: [
                .headerSearchPath("../cmark/src"),
                .headerSearchPath("../cmark-config")
            ]
        ),
        .testTarget(
            name: "contornoTests",
            dependencies: ["contorno", "libcmark"],
            resources: [
                .copy("Resources")
            ]
        ),
    ]
)
